﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace T9Test.Core.UnitTests
{
    [TestFixture]
    public class T9ConverterTests
    {
        private readonly T9Converter _t9Converter;

        public T9ConverterTests()
        {
            _t9Converter = new T9Converter();
        }

        [TestCase("yes", "999337777")]
        [TestCase("hi", "44 444")]
        [TestCase("foo", "333666 666")]
        [TestCase("foo  bar", "333666 6660 022 2777")]
        [TestCase("hello world", "4433555 555666096667775553")]
        [TestCase(null, "")]
        [TestCase("", "")]
        public void ConverterTest(string input, string output)
        {
            var result = _t9Converter.ConvertFrom(input);
            Assert.AreEqual(result, output);
        }

        [TestCase('y', "999")]
        [TestCase('e', "33")]
        [TestCase('s', "7777")]
        [TestCase('a', "2")]
        [TestCase('b', "22")]
        [TestCase('c', "222")]
        [TestCase('d', "3")]
        [TestCase('g', "4")]
        [TestCase('j', "5")]
        [TestCase('m', "6")]
        [TestCase('p', "7")]
        [TestCase('t', "8")]
        [TestCase('w', "9")]
        public void LetterConvertTest(char symbol, string output)
        {
            var result = _t9Converter.ConvertLetter(symbol);
            Assert.AreEqual(result, output);
        }

        [TestCase('A')]
        [TestCase('~')]
        [TestCase(default(char))]
        public void TestWrongSymbols(char symbol)
        {
            Assert.Throws<ArgumentException>(() => _t9Converter.ConvertLetter(symbol));
        }
    }
}
