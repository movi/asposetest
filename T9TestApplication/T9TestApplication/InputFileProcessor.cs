﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T9Test.Core;

namespace T9TestApplication
{
    /// <summary>
    /// Prepares output files for google code jam ("t9 spelling")
    /// </summary>
    internal class InputFileProcessor
    {
        private readonly T9Converter _converter;

        public InputFileProcessor()
        {
            _converter = new T9Converter();
        }

        public void Processfile(string fileName)
        {
            var builder = new StringBuilder();
            var lines = File.ReadAllLines(fileName).Skip(1);
            int j = 1;
            foreach (var line in lines)
            {
                var output = _converter.ConvertFrom(line);
                builder.Append(string.Format("Case #{0}: ", j));
                builder.AppendLine(output);
                j++;
            }
            string result = builder.ToString().Replace("\r\n", "\n");
            File.WriteAllText(fileName+".out", result);
        }
    }
}
