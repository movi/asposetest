﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using T9Test.Core;

namespace T9TestApplication
{
    /// <summary>
    /// Reads console input in an infinite loop
    /// writing output t9 sequenses
    /// </summary>
    internal class ConsoleReader
    {
        private T9Converter _converter;

        public ConsoleReader()
        {
            _converter = new T9Converter();
        }

        public void Run()
        {
            bool exit = false;
            do
            {
                Console.WriteLine("--------- Enter your message or type \"/end\" -------------------");
                string line = Console.ReadLine();

                if (string.Equals(line, "/end", StringComparison.OrdinalIgnoreCase))
                {
                    exit = true;
                }
                else
                {
                    try
                    {
                        var result = _converter.ConvertFrom(line);
                        Console.WriteLine(result);
                    }
                    catch (ArgumentException ex)
                    {
                        Console.WriteLine("Input string was incorrect:");
                        Console.WriteLine(ex.Message);
                    }
                    Console.WriteLine("---------------------------------------------------------------");
                }
            }
            while (!exit);
        }
    }
}
