﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9TestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var consoleReader = new ConsoleReader();
            consoleReader.Run();
        }
    }
}
