﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T9Test.Core
{
    public class T9Converter
    {
        private readonly List<int> _startingIndicies;
        
        /// <summary>
        /// letters on t-9 keyboards start with digit '2'
        /// </summary>
        const int FirstDigitForA = 2;

        public T9Converter()
        {
            _startingIndicies = new List<int>()
            {
                0,
                3,
                6,
                9,
                12,
                15,
                19,
                22,
                26
            };
        }

        /// <summary>
        /// Converts an input message string to the output
        /// t9-based string of digits and spaces
        /// </summary>
        /// <param name="input">
        /// input message
        /// </param>
        /// <returns>
        /// converted t9-based message
        /// </returns>
        public string ConvertFrom(string input)
        {
            if (String.IsNullOrEmpty(input)) return ""; //TODO: it depends on a consumer side what to send if input is empty

            StringBuilder sb = new StringBuilder();
            int prevSymbol = -1;
            for (var index = 0; index < input.Length; index++)
            {
                char t = input[index];

                string converted = ConvertLetter(t);

                //TODO: the knowledge about the last number may go by out int param in ConvertLetter for example
                int currentSymbolIndex = converted[0];

                //rule for the case when the space is needed
                if (currentSymbolIndex == prevSymbol)
                    sb.Append(' ');

                sb.Append(converted);
                prevSymbol = currentSymbolIndex;
            }
            return sb.ToString();
        }

        /// <summary>
        /// Converts an input symbol of message string to the output
        /// t9-based string of corresponding repeated number
        /// </summary>
        /// <param name="symbol">
        /// input character
        /// </param>
        /// <returns>
        /// converted t9-based string
        /// </returns>
        /// <exception cref="System.ArgumentException">
        /// throws exception if an input symbol
        /// wasn't in a 'a'-'z' range or wasn't a space symbol
        /// </exception>
        internal string ConvertLetter(char symbol)
        {
            if (symbol.Equals(' ')) return "0";
            if (symbol > 'z' || symbol < 'a') throw new ArgumentException("use small a to z characters in a message");

            int letterIndex = (symbol - 'a');
            int numberIndex = _startingIndicies.BinarySearch(letterIndex);
            numberIndex = CorrectFoundIndex(numberIndex, _startingIndicies.Count);
            if (numberIndex < 0) return "";

            int letterDiff = letterIndex - _startingIndicies[numberIndex];

            char number = (char) (FirstDigitForA + numberIndex + '0'); // or it can be int.ToString()[0]

            //if the difference is zero then we should press a key once
            var repeatsCount = letterDiff + 1;

            var result = new String(number, repeatsCount);
            return result;
        }

        /// <summary>
        /// helper method for correcting found negative index
        /// </summary>
        /// <param name="i"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        private int CorrectFoundIndex(int i, int length)
        {
            if (i >= 0)
            {
                return i;
            }

            int nearest = ~i;

            if (nearest == length || nearest == 0)
            {
                return -1;
            }

            return nearest - 1;
        }
    }
}
